===============================
Account Sale Weight Scenario
===============================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from decimal import Decimal
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax, create_tax_code

Install account_invoice_weight::

    >>> config = activate_modules('account_invoice_weight')

reate company::

    >>> _ = create_company()
    >>> company = get_company()

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> account_category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('20')
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products

Create invoice::

    >>> Invoice = Model.get('account.invoice')
    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> invoice = Invoice()
    >>> invoice.party = party
    >>> invoice.save()

Add line::

    >>> line = invoice.lines.new()
    >>> line.type = 'line'
    >>> line.product = product
    >>> line.price_unit = 'weight'
    >>> line.weight_uom = kg
    >>> line.quantity = 1620

When weight_price is changed unit_price is loaded::

    >>> line.weight = 1469
    >>> line.weight_price = Decimal('0.585')
    >>> line.unit_price
    Decimal('0.5305')
    >>> line.amount
    Decimal('859.41')
    >>> invoice.save()
    >>> invoice.reload()
    >>> line, = invoice.lines    
    >>> line.unit_price
    Decimal('0.5305')
    >>> line.amount
    Decimal('859.41')
    >>> line.weight_price
    Decimal('0.5850')
    >>> line.weight_price = Decimal('1.17')
    >>> line.unit_price
    Decimal('1.0609')
    >>> line.amount
    Decimal('1718.66')