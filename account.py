# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pyson import Eval, Bool, Id
from trytond.pool import Pool, PoolMeta
from decimal import Decimal
from trytond.modules.product import price_digits


def weight_mixin(workflow_field):
    "Returns a mixin to add weight fields to a model"

    class WeightMixin(object):
        _state = '%s_state' % workflow_field

        weight = fields.Float('Weight',
            digits=(16, Eval('weight_unit_digits', 2)),
            states={
                'readonly': Eval(_state) != 'draft',
                'invisible': Bool(Eval('weight_invisible'))
            },
            depends=['weight_unit_digits', _state,
                'weight_invisible'])
        weight_uom = fields.Many2One('product.uom', 'Weight UOM',
            select=True, ondelete='RESTRICT',
            domain=[('category', '=', Id('product', 'uom_cat_weight'))],
            states={
                'readonly': Eval(_state) != 'draft',
                'invisible': Bool(Eval('weight_invisible'))
            },
            depends=[_state])
        weight_unit_digits = fields.Function(
            fields.Integer('Weight unit digits'),
            'on_change_with_weight_unit_digits')
        weight_invisible = fields.Function(
            fields.Boolean('Weight invisible'),
            'on_change_with_weight_invisible')
        weight_price = fields.Function(
            fields.Numeric('Weight price',
                digits=price_digits,
                states={
                    'readonly': Eval(_state) != 'draft',
                    'invisible': Bool(Eval('weight_invisible'))
                },
                depends=[_state, 'weight_invisible']),
            'get_weight_price', setter='set_weight_price')
        price_unit = fields.Selection([
            ('weight', 'Weight'),
            ('uom', 'UOM')], 'Price unit',
            states={
                'readonly': Eval(_state) != 'draft',
                'invisible': Bool(Eval('weight_invisible'))
            },
            depends=[_state, 'weight_invisible'])

        @classmethod
        def __setup__(cls):
            super(WeightMixin, cls).__setup__()
            cls.unit_price.states['readonly'] |= (
                Eval('price_unit') == 'weight')
            cls.unit_price.depends.append('price_unit')

        @staticmethod
        def default_weight_invisible():
            return True

        @staticmethod
        def default_price_unit():
            return 'uom'

        def get_weight_price(self, name):
            if (self.weight is None or self.weight_uom is None):
                return None
            digits = self.__class__.weight_price._field.digits[1]
            return self.get_computed_price(self.unit, self.unit_price,
                self.weight_uom, digits)

        def get_computed_price(self, from_uom, price, to_uom,
                digits, reverse=False):
            Uom = Pool().get('product.uom')

            factor, rate = None, None
            if self.weight:
                rate = self.quantity / self.weight
                if self.unit and rate and self.weight_uom:
                    if self.unit.accurate_field == 'factor':
                        rate *= self.unit.factor
                    else:
                        rate /= self.unit.rate
                    if self.weight_uom.accurate_field == 'factor':
                        rate /= self.weight_uom.factor
                    else:
                        rate *= self.weight_uom.rate
            if self.quantity:
                factor = self.weight / self.quantity
                if self.weight_uom and factor and self.unit:
                    if self.weight_uom.accurate_field == 'factor':
                        factor *= self.weight_uom.factor
                    else:
                        factor /= self.weight_uom.rate
                    if self.unit.accurate_field == 'factor':
                        factor /= self.unit.factor
                    else:
                        factor *= self.unit.rate

            if price is None:
                return None
            elif from_uom == to_uom:
                return Uom.compute_price(
                    from_uom, price, to_uom,
                    ).quantize(Decimal(10) ** -Decimal(digits))
            elif from_uom and to_uom and (factor or rate):
                return Uom.compute_price(
                    from_uom, price, to_uom,
                    factor=factor if not reverse else rate,
                    rate=rate if not reverse else factor
                    ).quantize(Decimal(10) ** -Decimal(digits))
            else:
                return None

        @classmethod
        def set_weight_price(cls, records, name, value):
            pass

        @fields.depends('unit')
        def on_change_with_weight_invisible(self, name=None):
            pool = Pool()
            Modeldata = pool.get('ir.model.data')

            if self.unit:
                return self.unit.category.id == Modeldata.get_id(
                    'product', 'uom_cat_weight')
            return True

        def on_change_product(self):
            super(WeightMixin, self).on_change_product()
            self.weight_invisible = self.on_change_with_weight_invisible()
            if self.product:
                self.price_unit = self.product.price_unit
            # TODO: compute weight from product measurements

        @fields.depends('weight_uom')
        def on_change_with_weight_unit_digits(self, name=None):
            if self.weight_uom:
                return self.weight_uom.digits
            return 2

        @fields.depends('type', 'quantity', 'weight_price', 'weight',
            'unit', workflow_field, '_parent_%s.currency' % workflow_field,
            'weight_uom', 'unit')
        def on_change_weight_price(self):
            if not self.weight_price or not self.weight or not self.quantity:
                return None
            self.compute_quantity_from_weight()
            self.amount = self.on_change_with_amount()

        def compute_quantity_from_weight(self, currency=None, rounded=False):
            Uom = Pool().get('product.uom')

            amount = Decimal(str(self.weight)
                ) * (self.weight_price or Decimal('0.0'))
            rounded_amount = amount

            if not currency:
                currency = (getattr(self, workflow_field, None) or self
                    ).currency

            if currency and rounded:
                rounded_amount = currency.round(amount)

            digits = self.__class__.unit_price.digits[1]
            # this should be equal but discount module breaks it
            weight_digits = self.__class__.weight_price.digits[1]

            def _match_amount(pattern_amount, quantity, currency):
                unit_price = Decimal(pattern_amount / Decimal(str(quantity))
                    ).quantize(Decimal(10) ** -Decimal(digits))
                new_amount = Decimal(str(quantity or '0.0')) * \
                    (unit_price or Decimal('0.0'))

                if currency:
                    pattern_amount = currency.round(pattern_amount)
                    new_amount = currency.round(new_amount)
                return pattern_amount == new_amount

            def _match_price(pattern_amount, quantity):
                unit_price = (pattern_amount / Decimal(str(quantity))
                    ).quantize(Decimal(10) ** -Decimal(digits))
                amount = unit_price * Decimal(str(quantity))
                _price = (amount / Decimal(str(self.weight))
                    ).quantize(Decimal(10) ** -Decimal(weight_digits))
                if _price:
                    return _price == self.weight_price
                return False

            if not _match_amount(rounded_amount, self.quantity, currency) or \
                    not _match_price(amount, self.quantity):
                # try to increase uom
                uoms = Uom.search([
                    ('category', '=', self.unit.category.id),
                    ('factor', '>', self.unit.factor)],
                    order=[('factor', 'ASC')])
                uom, qty = self.unit, self.quantity
                found = False
                for uom in uoms:
                    qty = Uom.compute_qty(self.unit, self.quantity,
                        uom)
                    if not qty:
                        break
                    if _match_amount(rounded_amount, qty, currency) \
                            and _match_price(amount, qty):
                        found = True
                        break
                if found and Uom.compute_qty(
                        uom, qty, self.unit) == self.quantity:
                    self.unit = uom
                    self.unit_digits = self.on_change_with_unit_digits()
                    self.quantity = qty
                elif not rounded:
                    self.compute_quantity_from_weight(currency, rounded=True)
                    return

            self.unit_price = self.get_computed_price(
                self.weight_uom, self.weight_price, self.unit, digits,
                reverse=True)

    return WeightMixin


class InvoiceLine(weight_mixin('invoice'), metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @fields.depends('currency')
    def on_change_weight_price(self):
        super(InvoiceLine, self).on_change_weight_price()


class InvoiceLineIntrastat(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @property
    def intrastat_weight(self):
        pool = Pool()
        Uom = pool.get('product.uom')
        Modeldata = pool.get('ir.model.data')
        kg_uom = Uom(Modeldata.get_id('product', 'uom_kilogram'))

        if self.weight_uom and self.weight is not None:
            return Uom.compute_qty(self.weight_uom, self.weight, kg_uom)
        return super().intrastat_weight
