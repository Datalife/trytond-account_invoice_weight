# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import account
from . import purchase
from . import sale
from . import product


def register():
    Pool.register(
        account.InvoiceLine,
        product.Template,
        product.ProductPriceUnit,
        product.Product,
        module='account_invoice_weight', type_='model')
    Pool.register(
        sale.SaleLine,
        module='account_invoice_weight', type_='model',
        depends=['sale'])
    Pool.register(
        purchase.PurchaseLine,
        module='account_invoice_weight', type_='model',
        depends=['purchase'])
    Pool.register(
        account.InvoiceLineIntrastat,
        module='account_invoice_weight', type_='model',
        depends=['intrastat'])
