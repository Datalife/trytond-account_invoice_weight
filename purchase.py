# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from .account import weight_mixin


class PurchaseLine(weight_mixin('purchase'), metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def get_invoice_line(self):
        lines = super().get_invoice_line()
        for line in lines:
            line.price_unit = self.price_unit
            line.weight_uom = self.weight_uom
            if self.weight:
                factor = self.quantity / line.quantity
                line.weight = self.weight_uom.round(
                    self.weight * factor)
        return lines
